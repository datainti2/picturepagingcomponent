import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as _ from 'underscore';

@Component({
  selector: 'app-picturepaging',
  template: '<div class="col-md-8 col-md-offset-2"><md-grid-list cols="4"><md-grid-tile *ngFor="let item of pagedItems"><img src="{{item.img}}" class="img-rounded" alt="Cinque Terre" width="204" height="186"></md-grid-tile></md-grid-list><div class="pull-right"><ul *ngIf="pager.pages && pager.pages.length" class="pagination"><li [ngClass]="{disabled:pager.currentPage === 1}"><a (click)="setPage(pager.currentPage - 1)">Prev</a></li><li [ngClass]="{disabled:pager.currentPage === pager.totalPages}"><a (click)="setPage(pager.currentPage + 1)">Next</a></li></ul></div></div>',
  styleUrls: ['./base-widget.component.css']
})

export class BaseWidgetComponent implements OnInit {
  constructor(private http: Http) { }

    // array of all items to be paged
    private allItems: any[];

    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[];

    ngOnInit() {
          this.allItems = [
          {"img":"../../assets/img/images1.jpg"},
          {"img":"../../assets/img/images2.jpg"},
          {"img":"../../assets/img/images3.jpg"},
          {"img":"../../assets/img/images.jpg"},
          {"img":"../../assets/img/noimg.png"}, 
          {"img":"../../assets/img/anonymous.png"},
          {"img":"../../assets/img/v-twitter-3.jpg"},
          {"img":"../../assets/img/noimg.png"}];
          this.setPage(1);
    }
    getPager(totalItems: number, currentPage: number = 1, pageSize: number = 8) {
            // calculate total pages
            let totalPages = Math.ceil(totalItems / pageSize);

            let startPage: number, endPage: number;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }

            // calculate start and end item indexes
            let startIndex = (currentPage - 1) * pageSize;
            let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            let pages = _.range(startPage, endPage + 1);

            // return object with all pager properties required by the view
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            };
        }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.getPager(this.allItems.length, page);

        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

}
